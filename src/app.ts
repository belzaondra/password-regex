const main = () => {
  const password = process.argv[2];
  let re = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]){6}");

  if (re.exec(password)) console.log("password is valid");
  else console.log("password is invalid");
};

main();
